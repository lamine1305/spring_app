package dz.lamine;

import dz.lamine.entity.User;
import dz.lamine.service.UserService;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class AppDockerizeApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx= SpringApplication.run(AppDockerizeApplication.class, args);
		User us =new User("MEHIDI","Lamine","LM0584BL");
		UserService userService = ctx.getBean(UserService.class);
		userService.AddUser(us);
		us =new User("MEHIDI","Adam","DA0584GH");
		userService.AddUser(us);
	}

}
