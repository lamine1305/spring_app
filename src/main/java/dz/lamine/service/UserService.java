package dz.lamine.service;

import dz.lamine.entity.User;
import dz.lamine.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public void AddUser(User user ){
        userRepository.save(user);
    }

    public void deleteUser(Integer userID ){
        userRepository.deleteById(userID);
    }

    public List<User> listUser(){
        System.out.println("APPEL SERVICE");
       return userRepository.findAll();
    }

}
