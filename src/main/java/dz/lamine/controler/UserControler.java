package dz.lamine.controler;

import dz.lamine.entity.User;

import dz.lamine.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserControler {


    @Autowired
    UserService userService ;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<User> findAllUsers(){
        System.out.println("APPEL CONTROLER");
        return userService.listUser();
    }

    @PostMapping (value = "/add")
    @ResponseBody
    public void addUser(User user){
         userService.AddUser(user);
    }
}
